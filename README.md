SSHGuard
========
SSHGuard protects hosts from brute-force attacks against SSH and other
services. It aggregates system logs and blocks repeat offenders using
several firewall backends, including `iptables`, `ipfw`, and `pf`.

http://www.sshguard.net/
http://bitbucket.org/sshguard/sshguard/

Credits
-------
Authors: Mij <mij@bitchx.it>, T.J. Jones <tjjones03@gmail.com>
